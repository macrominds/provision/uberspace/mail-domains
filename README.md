# macrominds/provision/uberspace/mail-domains

Prepares an U7 Uberspace server with mail domains: 

* register all specified (externally managed) domains with Uberspace

Say, you own the externally managed `my-domain.com` and an Uberspace `mydomain.uber.space` and you plan to  
point your MX-Records to your Uberspace after provisioning. Using this role will
register `my-domain.com` on `mydomain.uber.space`, so that the mailserver will work for this domain.

**Notice:** It will **not** manage your DNS records! You need to do that manually or by using another tool.

**Warning:** It will exactly match the given mail domains. 
This means: mail domains that are currently registered with the Uberspace account, but not contained in your
`mail_domains_list` will be removed from the Uberspace mail domain list!

See https://gitlab.com/macrominds/provision/uberspace/web-domains for Uberspace web domain provisioning.

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

## Role Variables

See [defaults/main.yml](defaults/main.yml). 

* `mail_domains_list`: list of domains (domains will be added and removed for an exact match)
  Defaults to `["my-domain.com"]` and should definitely be set.
* `mail_domains_user_facts_domains_yml_path`: path to userfacts file on remote site.
  Defaults to `"/opt/uberspace/userfacts/{{ ansible_facts.user_id }}/domains.yml"`
* `mail_domains_tool_chdir`: the folder from which to execute the following commands.
  Defaults to the user's home directory `"/home/{{ ansible_facts.user_id }}"`
* `mail_domains_tool_add_command`: the command to add a domain.
  Defaults to `"uberspace mail domain add"`
* `mail_domains_tool_del_command`: the command to remove a domain.
  Defaults to `"uberspace mail domain del"`
* `mail_domains_tool_command_warnings`: if warnings should be issued for typical command misuse like touch, ls, etc.
  This is used to turn off warnings during testing, because the mocked commands would trigger a warning. 
  Defaults to `true`

## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/mail-domains.git
  path: roles
  name: mail-domains
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Playbook

```yaml
---
- hosts: all
  roles:
    - role: mail-domains
      mail_domains_list:
        - my-custom-domain.com
        - my-other-custom-domain.com
```

## Testing

Test this role with `molecule test --all`.

We're working with mocked uberspace commands and with fixtures of the uberspace userfacts domain.yml file.
The tests show mostly, that the role works without errors in multiple scenarios (no changes,
combinations of addition, removal) and that the roles are idempotent.

This is why we copy a fixture in almost all `molecule/**/playbook.yml` files after having performed the 
actions. The domain.yml fixture files reflect the expected content of the userfacts domain.yml file.

## BTW: Common Uberspace E-Mail-Server-Settings

IMAP: {server}.uberspace.de:993 (ssl/tls, **not** starttls)
User: {mailbox}@{user}.uber.space

SMTP port 587 (starttls), server and user same as IMAP

## License

ISC

## Author Information

This role was created in 2019 by Thomas Praxl.
