from testinfra.host import Host


def test_removed_mail_and_web_com_domain(host):
    assert get_expected_entries() == ls_home_as_array(host)


def get_expected_entries():
    return ["only-mail.com"]


def ls_home_as_array(host: Host):
    return host.ansible(
        "command",
        "ls /home/ansible",
        check=False
    )["stdout_lines"]
