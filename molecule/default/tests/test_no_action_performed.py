from testinfra.host import Host


def test_nothing_has_been_touched(host):
    assert get_expected_entries() == ls_home_as_array(host)


def get_expected_entries():
    return ["mail-and-web.com", "only-mail.com"]


def ls_home_as_array(host: Host):
    return host.ansible(
        "command",
        "ls /home/ansible",
        check=False
    )["stdout_lines"]
