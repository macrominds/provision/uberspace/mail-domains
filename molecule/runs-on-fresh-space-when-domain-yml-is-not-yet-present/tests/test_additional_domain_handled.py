from testinfra.host import Host


def test_additional_domain_handled(host):
    assert get_expected_entries() == ls_home_as_array(host)


def get_expected_entries():
    return ["fresh-mail.com"]


def ls_home_as_array(host: Host):
    return host.ansible(
        "command",
        "ls /home/ansible",
        check=False
    )["stdout_lines"]
