from testinfra.host import Host


def test_additional_domain_handled(host):
    expected = get_expected_entries()
    actual = ls_home_as_array(host)
    assert len(expected) == len(actual)
    assert get_expected_entries() == actual


def get_expected_entries():
    return ["mail-and-web.com", "only-mail.com", "www.only-web.com"]


def ls_home_as_array(host: Host):
    return host.ansible(
        "command",
        "ls /home/ansible",
        check=False
    )["stdout_lines"]
